﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMovement : MonoBehaviour {

    public float moveSpeed;
    public Vector2 dir;
    Vector2 currentPos;
    bool play = true;
    Vector2 direction;

    public GameObject sbObject;
    private ScreenBounds screenBounds;

    void Start()
    {
        screenBounds = sbObject.GetComponent<ScreenBounds>();
        dir = Vector2.zero; // character spawns at 0,0
        InvokeRepeating("Start1", 0f, 5f);
    }
    void Start1()
    {
        play = true;
        direction = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-4.0f, 4.0f)); // random position
    }
    void Update()
    {
        currentPos = transform.position; // current position of character
        if (play == true)
        { // calculating direction
            dir = direction - currentPos;

            dir.Normalize(); // sets magnitude to 1
            play = false;
        }
        Vector2 target = dir * moveSpeed + currentPos;  // calculating target position
        transform.position = Vector2.Lerp(currentPos, target, Time.deltaTime); // movement from current position to target position
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        CancelInvoke(); // stop moving to target position
        direction = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-4.0f, 4.0f)); // provides another random position
        play = true;

    }
    void OnCollisionExit2D()
    {
        InvokeRepeating("Start1", 2f, 5f);
    }
}
