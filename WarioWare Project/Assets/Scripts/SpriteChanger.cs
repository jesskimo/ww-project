﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour
{

    public SpriteRenderer spriteRenderer;
    public Sprite stillSprite;
    public Sprite leftSprite;
    public Sprite rightSprite;
    public Sprite loseSprite;

    public GameObject sprite;

    void Start()
    {
        spriteRenderer.sprite = stillSprite;
    }

    void Update()
    {
        RandMovement randMovement = sprite.GetComponent<RandMovement>(); // Gets information from the RandMovement script

        if (MGSystem.singelton.play == true)
        {
            // This section checks if the sprite's target is less than its current position, ie. if it's moving left
            float difference = randMovement.dir.x - randMovement.currentPos.x;

            if (difference < 0)
            {
                spriteRenderer.sprite = leftSprite;
            }
            // If the difference >= 0, ie. if the sprite's moving right
            else
            {
                spriteRenderer.sprite = rightSprite;
            }
        }
        else
        {
            if(MGSystem.singelton.hasWon == true)
            {
                spriteRenderer.sprite = stillSprite;
            }
            else
            {
                spriteRenderer.sprite = loseSprite;
            }
        }
    }

}
