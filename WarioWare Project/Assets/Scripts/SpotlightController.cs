﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightController : MonoBehaviour
{

    private Vector3 mousePosition;
    private Rigidbody2D rb;
    private Vector2 mouseDirection;
    private float lightSpeed = 100f; // The speed at which the spotlight will follow the mouse

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}

    // Player loses if the sprite and spotlight aren't colliding
    private void OnTriggerExit2D(Collider2D other)
    {
        if(MGSystem.singelton.play == false)
        {
            return;
        }
        else
        {
            MGSystem.singelton.EndGame(false);
        }
    }

    void Update ()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseDirection = (mousePosition - transform.position).normalized; // Calculates distance from spotlight position to mouse position
        rb.velocity = new Vector2(mouseDirection.x * lightSpeed, mouseDirection.y * lightSpeed); // Spotlight moves at lightSpeed
	}

}
