﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandMovement : MonoBehaviour
{

    public float moveSpeed;
    public Vector2 dir;
    public Vector2 currentPos;
    bool moving = false;
    Vector2 direction;
    public float timer = 2.0f;
    public GameObject sbObject;
    private ScreenBounds screenBounds;

    void Start()
    {
        screenBounds = sbObject.GetComponent<ScreenBounds>();
        dir = Vector2.zero; // character spawns at 0,0
        InvokeRepeating("Initialise", 0f, 5f);
    }

    void Initialise()
    {
        moving = true;
        direction = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-4.0f, 4.0f)); // random position
    }

    void Update()
    {
        // Waits 2 seconds before executing the rest of the function
        timer -= Time.deltaTime;
        if (timer > 0)
        {
            return;
        }

        currentPos = transform.position; // current position of character
        if (moving == true)
        { // calculating direction
            dir = direction - currentPos;

            dir.Normalize(); // sets magnitude to 1
            moving = false;
        }
        Vector2 target = dir * moveSpeed + currentPos;  // calculating target position
        transform.position = Vector2.Lerp(currentPos, target, Time.deltaTime); // movement from current position to target position
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        CancelInvoke(); // stop moving to target position
        direction = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-4.0f, 4.0f)); // provides another random position
        moving = true;
    }

    void OnCollisionExit2D()
    {
        InvokeRepeating("Start2", 2f, 5f);
    }
}
