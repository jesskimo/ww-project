﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundChanger : MonoBehaviour
{

    public AudioClip lightSwitch;
    public AudioClip ticking;
    public AudioClip winSound;
    public AudioClip loseSound;

    public AudioSource source; // The object that will play the sounds

    void Start()
    {
        source.GetComponent<AudioSource>();
        source.clip = lightSwitch;
        source.Play();
        Debug.Log("Playing lightSwitch");
    }

    void Update()
    {
        if(MGSystem.singelton.play == true)
        {
            source.clip = ticking;
            source.Play();
            Debug.Log("Playing ticking");
        }
        else
        {
            if (MGSystem.singelton.hasWon == true)
            {
                // hasWon is true when the game starts, so this makes sure the game has ended before playing the sound
                if(MGSystem.singelton.secondsLeft == 0)
                {
                    source.clip = winSound;
                    source.Play();
                    Debug.Log("Playing winSound");
                }
                else
                {
                    return;
                }
            }
            else
            {
                source.clip = loseSound;
                source.Play();
                Debug.Log("Playing loseSound");
            }
        }
    }

}
