﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MGSystem : MonoBehaviour
{

    public RandMovement randMovement;
    public int secondsLeft;
    public Text timerTxt;

    public GameObject gameStartTxt;
    public Text gameEndTxt;
    public GameObject gameEndObject;

    public Collider2D spotlightCol;
    public Collider2D spriteCol;

    public bool play;
    public bool hasWon = true;

    // The colour of gameEndTxt will change to one of these depending on the outcome of the game
    public Color winColor;
    public Color loseColor;

    public static MGSystem singelton = null; // Allows variables and functions to be used in other scripts

    // Shows text for 2 seconds before starting the game
    public IEnumerator Start()
    {
        if (singelton == null)
            singelton = this;
        play = false;
        gameStartTxt.SetActive(true);
        yield return new WaitForSeconds(2f);
        gameStartTxt.SetActive(false);
        StartCoroutine(Play());
    }

    public IEnumerator Play()
    {
        play = true;
        // Decreases number of seconds left by 1 after a second passes
        while (secondsLeft > 0)
        {
            yield return new WaitForSeconds(1f);
            secondsLeft--;
            timerTxt.text = secondsLeft.ToString();

            // The loop stops if the player loses
            if (play == false)
            {
                break;
            }
        }

        // Player wins if the timer reaches 0
        if(secondsLeft == 0)
        {
            EndGame(true);
        }
    }

    public void EndGame(bool win)
    {
        hasWon = win;
        play = false;
        randMovement.enabled = false; // Stops the sprite from moving

        // If the player has won
        if(win == true)
        {
            gameEndTxt.text = "You caught her!";
            gameEndTxt.color = winColor;
            gameEndObject.SetActive(true); // Displays above text
        }
        // If the player has lost
        else
        {
            hasWon = false;
            gameEndTxt.text = "You lost her...";
            gameEndTxt.color = loseColor;
            gameEndObject.SetActive(true);
        }
    }

}
